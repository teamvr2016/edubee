﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelIconCreate : MonoBehaviour {

	// Use this for initialization
    public GameObject unlockedIcon, lockedIcon;
    public Camera camera;
	void Start () 
    {
        int count = 1;
        for (float j = 0.85f; j >0; )
        {
            for (float i = 0.1f; i < 1; )
            {
               // Vector3 p = camera.ScreenToWorldPoint(new Vector3(Screen.width * i, Screen.height * j, camera.nearClipPlane));
                //unlockedIcon.transform.position = p;
                unlockedIcon.GetComponent<LevelIconSize>().camera = camera;
                unlockedIcon.GetComponent<LevelIconSize>().widthFactor = i;
                unlockedIcon.GetComponent<LevelIconSize>().heightFactor = j;
                unlockedIcon.GetComponentInChildren<LevelText>().question = "" + count;
                unlockedIcon.GetComponentInChildren<LevelText>().stringValue =  count;
                // = camera;
                Instantiate(unlockedIcon);
                i += 0.16f;
                count++;
            }
            j -= 0.25f;
        }
        
        
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
