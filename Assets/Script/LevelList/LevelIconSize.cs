﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(SpriteRenderer))]

public class LevelIconSize : MonoBehaviour
{

    public float factor;
    public float increaseFactor;
    public bool increase = true;
    public float scale;
    float high, low;
    // public int x; 
    public Camera camera;
    public float widthFactor, heightFactor;

    void Start()
    {

        Resize();
    }

    void FixedUpdate()
    {
        animate();
    }

    void animate()
    {
        if (increase)
        {
            Vector3 imgScale = new Vector3(scale, scale, scale);
            imgScale.x = transform.localScale.x + increaseFactor;
            imgScale.y = transform.localScale.y + increaseFactor;
            transform.localScale = imgScale;

            if (imgScale.y >= high)
            {
                increase = false;
            }
        }
        else
        {
            Vector3 imgScale = new Vector3(scale, scale, scale);
            imgScale.x = transform.localScale.x - increaseFactor;
            imgScale.y = transform.localScale.y - increaseFactor;
            transform.localScale = imgScale;

            if (imgScale.y <= low)
            {
                increase = true;
            }
        }

        //camera = GetComponent<Camera>();
        // Vector3 p = camera.ScreenToWorldPoint(new Vector3(Screen.width/2, Screen.height/4, camera.nearClipPlane));
        //transform.position = p;

    }

    void Resize()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        transform.localScale = new Vector3(scale, scale, scale);

        // example of a 640x480 sprite
        float width = sr.sprite.bounds.size.x * 100; // 4.80f
        float height = sr.sprite.bounds.size.y * 100; // 6.40f

        // and a 2D camera at 0,0,-10
        //  float worldScreenHeight = Camera.main.orthographicSize * 2f; // 10f
        //  float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width; // 10f

        Vector3 imgScale = transform.localScale;

        imgScale.x = (factor * Screen.width) / width;
        imgScale.y = (factor * Screen.width) / width;

        high = imgScale.x + 0.05f;
        low = imgScale.x - 0.05f;
        transform.localScale = imgScale;


        //camera = GetComponent<Camera>();
        Vector3 p = camera.ScreenToWorldPoint(new Vector3(Screen.width * widthFactor, Screen.height * heightFactor, camera.nearClipPlane));
        transform.position = p;
    }
}
