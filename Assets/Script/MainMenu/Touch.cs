﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Touch : MonoBehaviour
{

    public string clickevent;
    public GameObject touchEffect;
    public AudioClip touchSound;
    public GameObject WrongPrefab;
    public GameObject RightPrefab;
    // Use this for initialization

    // to detect touch on a gameobject, shei game object e must collider2D thaka lagbe

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        // for android
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            gameObject.GetComponent<AudioSource>().Play();
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint((Input.GetTouch(0).position)), Vector2.zero);
            if (hit.collider != null)
            {
                if (hit.collider.tag == "Player")
                {
                    Debug.Log("Touched it");
                }

                else if (hit.collider.tag == "bee")
                {
                    GameObject onj = hit.collider.gameObject;
                    Vector2 pos = onj.transform.position;
                    Instantiate(touchEffect, pos, onj.transform.rotation);
                    Destroy(onj);

                }

                else if (hit.collider.tag == "title")
                {
                    GameObject onj = hit.collider.gameObject;
                    Vector2 pos = onj.transform.position;
                    Instantiate(touchEffect, pos, onj.transform.rotation);
                    //   Destroy(onj);

                }

                else if (hit.collider.tag == "cloud")
                {
                    GameObject onj = hit.collider.gameObject;
                    Vector2 pos = onj.transform.position;
                    Instantiate(touchEffect, pos, onj.transform.rotation);
                    SpriteText spriteText = (SpriteText)onj.GetComponentInChildren<SpriteText>();
                    if (spriteText.stringValue == GetComponent<GameType>().gameType)
                    {
                        Destroy(onj);
                        RightPrefab.GetComponent<playIconSize>().camera = Camera.main;
                        Instantiate(RightPrefab);
                        GetComponent<PointControl>().point += 1;
                    }

                    else
                    {
                        Destroy(onj);
                        WrongPrefab.GetComponent<playIconSize>().camera = Camera.main;
                        Instantiate(WrongPrefab);
                    }


                }

                else if (hit.collider.tag == "level")
                {
                    GameObject onj = hit.collider.gameObject;
                    Vector2 pos = onj.transform.position;
                    Instantiate(touchEffect, pos, onj.transform.rotation);
                    int levelId = onj.GetComponentInChildren<LevelText>().stringValue;

                    if (levelId == 1)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene("TenseGame1");
                    }

                    else
                    {
                        Destroy(onj);
                    }

                }
            }
        }

            // for windows pc
            if (Input.GetMouseButtonDown(0))
            {
                gameObject.GetComponent<AudioSource>().Play();
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                Debug.Log("check");
                if (hit != false)
                {
                    Debug.Log("object clicked: " + hit.collider.tag);
                    if (hit.collider.tag == "Player")
                    {

                        UnityEngine.SceneManagement.SceneManager.LoadScene(clickevent);

                    }
                    else if (hit.collider.tag == "bee")
                    {
                        GameObject onj = hit.collider.gameObject;
                        Vector2 pos = onj.transform.position;
                        Instantiate(touchEffect, pos, onj.transform.rotation);
                        Destroy(onj);

                    }

                    else if (hit.collider.tag == "title")
                    {
                        GameObject onj = hit.collider.gameObject;
                        Vector2 pos = onj.transform.position;
                        Instantiate(touchEffect, pos, onj.transform.rotation);
                        // Destroy(onj);

                    }
                    else if (hit.collider.tag == "cloud")
                    {
                        GameObject onj = hit.collider.gameObject;
                        Vector2 pos = onj.transform.position;
                        Instantiate(touchEffect, pos, onj.transform.rotation);
                        SpriteText spriteText = (SpriteText)onj.GetComponentInChildren<SpriteText>();
                        if (spriteText.stringValue == GetComponent<GameType>().gameType)
                        {
                            Destroy(onj);
                            RightPrefab.GetComponent<playIconSize>().camera = Camera.main;
                            Instantiate(RightPrefab);
                            GetComponent<PointControl>().point += 1;
                        }

                        else
                        {
                            Destroy(onj);
                            WrongPrefab.GetComponent<playIconSize>().camera = Camera.main;
                            Instantiate(WrongPrefab);
                        }


                    }

                    else if (hit.collider.tag == "level")
                    {
                        GameObject onj = hit.collider.gameObject;
                        Vector2 pos = onj.transform.position;
                        Instantiate(touchEffect, pos, onj.transform.rotation);
                        int levelId = onj.GetComponentInChildren<LevelText>().stringValue;

                        if (levelId == 1)
                        {
                            UnityEngine.SceneManagement.SceneManager.LoadScene("TenseGame1");
                        }

                        else
                        {
                            Destroy(onj);
                        }

                    }
                    /*
                                    else if (hit.collider.tag == "background")
                                    {
                                        Debug.Log("Tiash");
                                        Vector2 wantedScreenSpawnPos = new Vector2 (Input.mousePosition.x, Screen.height - Input.mousePosition.y);
                                        Vector2 worldPos = Camera.main.ScreenToWorldPoint(new Vector3(wantedScreenSpawnPos.x, wantedScreenSpawnPos.y));
                                        Instantiate(touchEffect, worldPos,Quaternion.identity);
                                    }
                                    */
                }
                /*    else
                    {
                        Debug.Log("Checking touch");
               
                    }*/
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(clickevent);
            }
        }
    }
