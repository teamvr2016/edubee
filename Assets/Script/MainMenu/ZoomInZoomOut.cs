﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomInZoomOut : MonoBehaviour {

	// Use this for initialization
    public bool increase = true;

    public float factor;
    float high, low;
    public float increaseFactor;

	void Start () {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
       // = new Vector3(1, 1, 1);
        Vector2 scaler = transform.localScale;

        // example of a 640x480 sprite
        float width = sr.sprite.bounds.size.x * 100; // 4.80f
        float height = sr.sprite.bounds.size.y * 100; // 6.40f

        // and a 2D camera at 0,0,-10
        //  float worldScreenHeight = Camera.main.orthographicSize * 2f; // 10f
        //  float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width; // 10f

        Vector3 imgScale = new Vector3(1f, 1f, 1f);

        imgScale.x = (factor * Screen.width) / width;
        imgScale.y = (factor * Screen.width) / width;

        high = scaler.x + 0.05f;
        low = scaler.x - 0.05f;
	}
	

    void FixedUpdate()
    {
        animate();
    }




    void animate()
    {
        if (increase)
        {
            Vector3 imgScale = new Vector3(1f, 1f, 1f);
            imgScale.x = transform.localScale.x + increaseFactor;
            imgScale.y = transform.localScale.y + increaseFactor;
            transform.localScale = imgScale;

            if (imgScale.y >= high)
            {
                increase = false;
            }
        }
        else
        {
            Vector3 imgScale = new Vector3(1f, 1f, 1f);
            imgScale.x = transform.localScale.x - increaseFactor;
            imgScale.y = transform.localScale.y - increaseFactor;
            transform.localScale = imgScale;

            if (imgScale.y <= low)
            {
                increase = true;
            }
        }

    }
}
