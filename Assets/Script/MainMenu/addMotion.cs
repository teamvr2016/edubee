﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addMotion : MonoBehaviour {

    public float moveSpeed;

	void Start () 
    {
        Vector2 pos = transform.position;
        //Random rnd = new Random();
        float yyyy = GetRandomeNumber(-4.0f, 4.0f);
        pos.y = yyyy;
        transform.position = pos;
	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(moveSpeed, GetRandomeNumber(-0.05f,0.05f), 0);

        Vector2 pos = transform.position;
        if (pos.x < 15.0f && pos.x > -15.0f)
        {
           // Debug.Log("Alive");
        }
        else
        {
            Destroy(gameObject); 
            //Debug.Log("Dead");
        }
	}

    public float GetRandomeNumber(float minimum, float maximum)
    {
        return Random.Range(minimum, maximum);
    }


}
