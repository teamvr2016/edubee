﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudAddMotion : MonoBehaviour
{

    public float moveSpeed;
    
    void Start()
    {
        Vector2 pos = transform.position;
        float yyyy = GetRandomeNumber(-4.0f, 4.0f);
        pos.y = yyyy;
        transform.position = pos;
    }

    // Update is called once per frame
    void Update()
    {

        transform.Translate(moveSpeed, GetRandomeNumber(-0.05f, 0.05f), 0);

        Vector2 pos = transform.position;
        if (pos.x < 15.0f && pos.x > -15.0f)
        {
            // Debug.Log("Alive");
        }
        else
        {
            updatePoint();
           
        }
    }

    void updatePoint()
    {
        int gameType = GameObject.Find("Generator").GetComponent<GameType>().gameType;

        int selectedCloudType = gameObject.GetComponentInChildren<SpriteText>().stringValue;

        if (gameType != selectedCloudType)
        {
            int prev = GameObject.Find("Generator").GetComponent<PointControl>().point;
            GameObject.Find("Generator").GetComponent<PointControl>().point = prev + 1; 
            Destroy(gameObject);
        }
        else
        {
            Debug.Log("game : " + gameType + "and " + selectedCloudType); 
            Destroy(gameObject);
        }
    }

    public float GetRandomeNumber(float minimum, float maximum)
    {
        return Random.Range(minimum, maximum);
    }


}
