﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudGenerator : MonoBehaviour
{

    // Use this for initialization
    public float delay;
    public GameObject cloudPrefab;
    public int selectedType=0;
    string[] present = new string[] { "bear", "become", "begin", "bite", "break", "bring", "catch", "choose", "come", "do", "drink", "drive", "eat", "fall", "feel", "fly", "freeze", "get", "go", "know", "lay", "lead", "lend", "lie", "lose", "ride", "ring", "rise", "run", "say", "see", "set", "shake", "sing", "sink", "sit", "sleep", "speak", "steal", "swim", "take", "throw", "wear", "win", "write" };
    string[] past = new string[] { "bore", "became", "began", "bit", "broke", "brought", "caught", "chose", "came", "did", "drank", "drove", "ate", "fell", "felt", "flew", "froze", "got", "went", "knew", "laid", "led", "lent", "lay", "lost", "rode", "rang", "rose", "ran", "said", "saw", "set", "shook", "sang", "sank", "sat", "slept", "spoke", "stole", "swam", "took", "threw", "wore", "won", "wrote" };
    string[] pastparticiple = new string[] { "borne", "become", "begun", "bitten", "broken", "brought", "caught", "chosen", "come", "done", "drunk", "driven", "eaten", "fallen", "felt", "flown", "frozen", "gotten", "gone", "known", "laid", "led", "lent", "lain", "lost", "ridden", "rung", "risen", "run", "said", "seen", "set", "shaken", "sung", "sunk", "sat", "slept", "spoken", "stolen", "swum", "taken", "thrown", "worn", "won", "written" };
    void Start()
    {
        InvokeRepeating("CreateClouds", delay, delay);
    }


    void CreateClouds()
    {
        SpriteText spriteText = (SpriteText)cloudPrefab.GetComponentInChildren<SpriteText>();

        selectedType = Random.Range(0, 2);
        spriteText.stringValue = selectedType;

        int num = present.Length;
        int pos = Random.Range(0, num - 1);

        if (selectedType == 0)
            spriteText.question = present[pos];
        else if (selectedType == 1)
            spriteText.question = past[pos];
        else
            spriteText.question = pastparticiple[pos];
       
        ///////////////////////////////////////////////////////////////////////////////



        Vector2 posi = cloudPrefab.transform.position;
        float yyyy = GetRandomeNumber(-4.0f, 4.0f);
        posi.y = yyyy;
        //transform.position = posi;
        Instantiate(cloudPrefab);

       
    }

    public float GetRandomeNumber(float minimum, float maximum)
    {
        return Random.Range(minimum, maximum);
    }
}
