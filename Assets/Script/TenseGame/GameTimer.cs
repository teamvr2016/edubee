﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimer : MonoBehaviour {

	// Use this for initialization
    public int duration;
    public string question;

	void Start () 
    {
        Vector3 p = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.75f, Screen.height * 0.95f, 9f));
        transform.position = p;
        Renderer parentRenderer = gameObject.GetComponentInChildren<Renderer>();
        var renderer = GetComponent<Renderer>();
        renderer.sortingLayerID = parentRenderer.sortingLayerID;
        renderer.sortingOrder = parentRenderer.sortingOrder + 1;
        var spriteTransform = gameObject.transform;
        var text = GetComponent<TextMesh>();
        var pos = spriteTransform.position;
        question = "Time: " + duration;
        text.text = string.Format(question, pos.x, pos.y);

        InvokeRepeating("DecreaseTime", 1f, 1f);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (duration == 0)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MenuDesign");
        }
        var text = GetComponent<TextMesh>();
        var pos = gameObject.transform.position;
        question = "Time: " + duration;
        text.text = string.Format(question, pos.x, pos.y);
	}

    public void DecreaseTime()
    {
        duration--;
    }
}
