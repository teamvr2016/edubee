﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointControl : MonoBehaviour {

    public string question;
    public Camera camera;
    Renderer parentRenderer;
    public int point = 0;
    void Start()
    {
        Vector3 p = camera.ScreenToWorldPoint(new Vector3(Screen.width * 0.05f, Screen.height * 0.95f, 9f));
        transform.position = p;
        parentRenderer = gameObject.GetComponentInChildren<Renderer>();
        var renderer = GetComponent<Renderer>();
        renderer.sortingLayerID = parentRenderer.sortingLayerID;
        renderer.sortingOrder = parentRenderer.sortingOrder + 1;
        var spriteTransform = gameObject.transform;
        var text = GetComponent<TextMesh>();
        var pos = spriteTransform.position;
        question = "Point: " + point;
        text.text = string.Format(question, pos.x, pos.y);
    }

    void Update()
    {
        var text = GetComponent<TextMesh>();
        var pos = gameObject.transform.position;
        question = "Point: " + point;
        text.text = string.Format(question, pos.x, pos.y);
    }
}
