﻿using UnityEngine;
 
// empty gameobject e textMest component add kora lagbe
// text mesh e font size 100 r 
 public class SpriteText : MonoBehaviour
 {
     public string question;
     public int stringValue;
     void Start()
     {
         var parent = transform.parent;
         var parentRenderer = parent.GetComponent<Renderer>();
         var renderer = GetComponent<Renderer>();
         renderer.sortingLayerID = parentRenderer.sortingLayerID;
         renderer.sortingOrder = parentRenderer.sortingOrder+1;
         var spriteTransform = parent.transform;
         var text = GetComponent<TextMesh>();
         var pos = spriteTransform.position;
         text.text = string.Format(question, pos.x, pos.y);
     }

     void setQuestion(string sen)
     {
         question = sen;
     }
 }
