﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour {

    public bool leftRotate;
    public int speed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Rotate();
	}

    void Rotate()
    {
        if (leftRotate)
            transform.Rotate(Vector3.forward * speed);
        else
            transform.Rotate(Vector3.forward * speed* -1);
    }
}
